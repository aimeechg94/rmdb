import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
// Styles
import { Image, ThumbContainer, TitleBlock } from './Thumb.styles';

const Thumb = ({ image, movieId, movieTitle, clickable }) => (
  <ThumbContainer>
    {clickable ? (
      <Link to={`/${movieId}`}>
        <Image src={image} alt={movieTitle} />
      </Link>
    ) : (
      <Image src={image} alt={movieTitle} />
    )}
    <TitleBlock>{movieTitle}</TitleBlock>
  </ThumbContainer>
);

Thumb.propTypes = {
  image: PropTypes.string,
  movieId: PropTypes.number,
  clickable: PropTypes.bool,
};
export default Thumb;
